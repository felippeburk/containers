# ubuntu1804

pulled largely from buildpack-deps and python3.6 official docker files, but cleaned up a bit.

buildpack-deps:
https://github.com/docker-library/buildpack-deps/blob/cd0058f0893008c7ffa8e9cb9d3d5208cf5f2f75/bionic/Dockerfile

python3.6:
https://raw.githubusercontent.com/docker-library/python/ac47c1bc7bffe22af0c4193f1b1656ca07a24a97/3.6/stretch/Dockerfile
